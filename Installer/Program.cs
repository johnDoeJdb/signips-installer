﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Installer.Internals;
using Microsoft.Win32.TaskScheduler;
using Microsoft.Win32;

namespace Installer
{
    class Program
    {
        static string FilesFolder;
        static string SignIpsUpdatePath;
        static string XamppPath;
        static string pathToStartUpScript;
        static string pathToStartUpXampp;

        static void Main(string[] args)
        {
            InitVariables();

            InstallXAMPP();
            InstallChrome();
            InstallChromeExtension();            

            UnpackOpenhardware();
            UnpackCofigurator();
            UnpackUpdater();
            UnpackPlayer();

            RunOpenhardware();
            
            UpdatePath();
            AddTasksToScheduler();
            ConfigXammp();
            RestartXammp();
            DisableShell();
        }

        static private void InitVariables()
        {
            FilesFolder = @Path.GetFullPath("./files/");
            SignIpsUpdatePath = "D:\\signips-updater";
            XamppPath = "C:\\xampp";
            pathToStartUpScript = "D:\\signips-updater.exe";
            pathToStartUpXampp = "D:\\start_xampp.exe";
        }

        static private void UnpackOpenhardware()
        {
            string OpenhardwaremonitorPath = @"C:\\Openhardwaremonitor";
            try
            {
                string zipPath = @Path.Combine(FilesFolder, "openhardwaremonitor.zip");
                Directory.CreateDirectory(OpenhardwaremonitorPath);
                string extractPath = @Path.GetFullPath(OpenhardwaremonitorPath);
                UnZip(zipPath, extractPath);
            }
            catch { }
            
            Console.WriteLine("Successfuly unpacked openhardwaremonitor files");
        } 

        static private void UnpackUpdater()
        {
            try
            {
                string zipPath = Path.Combine(FilesFolder, "signips-updater.zip");
                string extractPath = @Path.GetFullPath("D:\\signips-updater");
                UnZip(zipPath, extractPath);
            }
            catch { }
            try
            {
                var process = new Process();
                process.StartInfo.FileName = "php";
                process.StartInfo.Arguments = "composer.phar install";
                process.StartInfo.WorkingDirectory = @SignIpsUpdatePath;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = false;
                process.Start();
            }
            catch {}
            try
            {
                File.Copy(Path.Combine(FilesFolder, "signips-updater.exe"), pathToStartUpScript);
            }
            catch {}
            
            Console.WriteLine("Successfuly unpacked updater files");
        }

        static private void UnpackCofigurator()
        {
            try
            {
                string inputPathConfigurator = Path.Combine(FilesFolder, "signips-configurator.exe");
                string ouputPathConfigurator = @Path.GetFullPath("D:\\");
                File.Copy(inputPathConfigurator, ouputPathConfigurator);
            }
            catch { }
            Console.WriteLine("Configurator was successfuly installed");
        }

        static private void UnpackPlayer()
        {
            try
            {
                string zipPath = Path.Combine(FilesFolder, "player.zip");
                string extractPath = @Path.GetFullPath("D:\\application");
                UnZip(zipPath, extractPath);
            }
            catch { }
            try
            {
                string playListDirPath = @"D:\\player";
                Directory.CreateDirectory(playListDirPath);
                File.Create(playListDirPath + "\\db_playlist.xml");
                File.Create(playListDirPath + "\\db_rss.xml");
                File.Create(playListDirPath + "\\location.txt");

                string contentsDirPath = playListDirPath + "\\contents";
                Directory.CreateDirectory(contentsDirPath);
                Directory.CreateDirectory(contentsDirPath + "\\templates");
                Directory.CreateDirectory(contentsDirPath + "\\media");
                Directory.CreateDirectory(contentsDirPath + "\\photos");
                Directory.CreateDirectory(contentsDirPath + "\\rss");
                Directory.CreateDirectory(contentsDirPath + "\\rss\\templates");
                Directory.CreateDirectory(contentsDirPath + "\\rss\\files");

                Console.WriteLine("Successfuly created playlist folder structure");
            }
            catch {}
            Console.WriteLine("Successfuly unpacked player application");
        }

        static private void InstallChromeExtension()
        {
            var registerKey = "Software\\Google\\Chrome\\Extensions\\lcdnelobbdifbhcefgofjgggeaomcplm";
            if (System.Environment.Is64BitOperatingSystem)
            {
                registerKey = "Software\\Wow6432Node\\Google\\Chrome\\Extensions\\lcdnelobbdifbhcefgofjgggeaomcplm";
            };
            RegistryKey MyReg = Registry.CurrentUser.CreateSubKey(registerKey);
            MyReg.SetValue("update_url", "http://localhost/application/updates.xml");
            Console.WriteLine("Google Chrome extension was installed\n");
        }

        static private void InstallChrome()
        {
            try
            {
                string chromePath = @Path.Combine(FilesFolder, "chrome_installer.exe");
                Process.Start(chromePath, "/silent /install");
                Console.WriteLine("Start downloading Google Chrome (WARNING: IT REQUIRE INTERNET CONNECTION!)\n");
                while (Process.GetProcessesByName("chrome_installer").Length != 0)
                {
                    //sleep 5 seconds
                    System.Threading.Thread.Sleep(5 * 1000);
                }
            }
            catch { }
            
            Console.WriteLine("Google Chrome succesfylly installed\n");
        }

        static private void InstallXAMPP()
        {
            try
            {
                File.Copy(Path.Combine(FilesFolder, "start_xampp.exe"), pathToStartUpXampp);
            }
            catch { }
            string pathToXAMPP = @Path.Combine(FilesFolder, "xampp-installer.exe");
            Process.Start(pathToXAMPP, "--mode unattended");
            Console.WriteLine("Start installing XAMPP...\n");
            while (Process.GetProcessesByName("xampp-installer").Length != 0)
            {
                //sleep 5 seconds
                System.Threading.Thread.Sleep(5 * 1000);
            }
            Console.WriteLine("XAMPP succesfylly installed\n");
        }

        static private void UpdatePath()
        {
            string PATH = Environment.GetEnvironmentVariable("PATH") + ";C:\\xampp\\php";
            Environment.SetEnvironmentVariable("PATH", PATH);
            Console.WriteLine("PATH was updated\n");
        }

        static private void RunOpenhardware()
        {
            string openHadwareMonitor = @Path.GetFullPath("C:\\Openhardwaremonitor\\OpenHardwareMonitor.exe");
            Process.Start(openHadwareMonitor);

            Console.WriteLine("OpenHadwareMonitor was runned\n");
        }

        static private void AddTasksToScheduler()
        {
            using (TaskService ts = new TaskService())
            {
                TaskDefinition startUpdaterTaskDefinition = ts.NewTask();
                startUpdaterTaskDefinition.RegistrationInfo.Description = "Start SignIPS updater";
                startUpdaterTaskDefinition.Triggers.Add(new LogonTrigger());
                startUpdaterTaskDefinition.Settings.Hidden = true;
                ExecAction powerOnAction = new ExecAction(pathToStartUpScript, null, null);
                startUpdaterTaskDefinition.Actions.Add(powerOnAction);
                ts.RootFolder.RegisterTaskDefinition(@"SignIPS poweron updater", startUpdaterTaskDefinition);

                TaskDefinition startChromeTaskDefinition = ts.NewTask();
                startChromeTaskDefinition.RegistrationInfo.Description = "Start Google Chrome";
                startChromeTaskDefinition.Triggers.Add(new LogonTrigger());
                ExecAction startChromeCheckAction = new ExecAction("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe", "--kiosk http://localhost/application/html/boot.html");
                startChromeTaskDefinition.Actions.Add(startChromeCheckAction);
                ts.RootFolder.RegisterTaskDefinition(@"Chrome start checker", startChromeTaskDefinition);
                Console.WriteLine("Successfuly added tasks to task scheduler\n");
            }
        }

        static private void ConfigXammp()
        {
            var confFileLocation = @Path.Combine(XamppPath, "apache\\conf\\httpd.conf");
            var xamppConfig = @Path.Combine(XamppPath, "xampp-control.ini");

            var fileContents = File.ReadAllText(confFileLocation);
            fileContents = fileContents.Replace("C:/xampp/htdocs", "D:/");
            File.WriteAllText(confFileLocation, fileContents);

            var config  = File.ReadAllText(xamppConfig);
            config = config.Replace("MySQL=1", "MySQL=0");
            config = config.Replace("FileZilla=1", "FileZilla=0");
            config = config.Replace("Mercury=1", "Mercury=0");
            config = config.Replace("Tomcat=1", "Tomcat=0");
            File.WriteAllText(xamppConfig, config);

            //application/x-shockwave-flash
            var mimTypesFile = "C:\\xampp\\apache\\conf\\mime.types";
            using (StreamWriter sw = File.AppendText(mimTypesFile))
            {
                sw.WriteLine("application/x-shockwave-flash .");
            }

            Console.WriteLine("XAMPP was successfuly configurated\n");
        }

        static private void RestartXammp() 
        {
            var xammpPath = @"C:\\xampp";
            var interval = 1 * 10 * 1000;

            Console.WriteLine("Stoping XAMPP...\n");
            Process.Start(Path.Combine(xammpPath, "xampp_stop.exe"));
            Thread.Sleep(interval);
            Console.WriteLine("XAMPP was stopped\n");

            Console.WriteLine("Starting XAMPP...\n");
            Process.Start(Path.Combine(xammpPath, "xampp_start.exe"));            
            Thread.Sleep(interval);
            Console.WriteLine("XAMPP was started\n");
        }

        static private void DisableShell()
        {
            RegistryKey regKey;
            // Change the Local Machine shell executable
            regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", true);
            regKey.SetValue("Shell", pathToStartUpXampp);
            regKey.Close();
            // Create the Shell executable Registry entry for Current User
            regKey = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows NT\CurrentVersion\Winlogon", true);
            regKey.SetValue("Shell", pathToStartUpXampp);
            regKey.Close();

            Console.WriteLine("Windows shell was successfuly disabled\n");
            Console.WriteLine("SingIPS Application was installed. Please restart computer to correctly run player\n");
            Console.ReadLine();
        }

        public static void UnZip(string zipFile, string folderPath)
        {
            using (var unzip = new Unzip(zipFile))
            {
                unzip.ExtractToDirectory(folderPath);
            }
        }

        public static void WriteToFile(string path, string content)
        {
            StreamWriter file = new StreamWriter(path);
            file.WriteLine(content);
            file.Close();
        }
    }
}
